package chat;

public class Main {
    public static void main(String[] args) {
        Thread thread1 = new Thread(new Consumer());
        thread1.start();

        Thread thread2 = new Thread(new Producer());
        thread2.start();
    }
}
